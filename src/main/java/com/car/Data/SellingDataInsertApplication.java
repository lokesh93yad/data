package com.car.Data;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;


import javax.persistence.Entity;


@SpringBootApplication
@EnableWebMvc
@EntityScan("com.car.Data.entity")
public class SellingDataInsertApplication {

	public static void main(String[] args) {
		SpringApplication.run(SellingDataInsertApplication.class, args);
	}

}
