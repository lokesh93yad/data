package com.car.Data.repository;

import com.car.Data.entity.Car;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface carRepository extends JpaRepository<Car,Integer> {


    Car findByName(String name);
}
