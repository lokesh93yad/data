package com.car.Data.controller;

import com.car.Data.entity.Car;
import com.car.Data.service.carService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class carController {
    @Autowired
    private carService service;

    @PostMapping("/addCar")
    public Car addCar(@RequestBody Car car){
        return service.saveCar(car);
    }

    @PostMapping("/addCars")
    public List<Car> addCars(@RequestBody List<Car> cars){
        return service.saveListOfCar(cars);
    }
    @GetMapping("/cars")
    public List<Car> findAllCars(){
        return service.getCar();
    }
    @GetMapping("/carbyid")
    public Car findCarById(@RequestParam int id){
        return service.getCarById(id);
    }
    @GetMapping("/carbyname")
    public Car findCarByName(@RequestParam String name){
        return service.getCarByName(name);
    }

    @PutMapping("/update")
    public Car updateCar(@RequestBody Car car){
        return service.updateCar(car);
    }
    @DeleteMapping("/delete")
    public String deleteCar(@RequestParam int id){
        return service.deleteCar(id);
    }
}
