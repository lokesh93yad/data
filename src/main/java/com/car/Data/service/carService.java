package com.car.Data.service;

import com.car.Data.entity.Car;
import com.car.Data.repository.carRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class carService {
    @Autowired
    private carRepository repository;

    public Car saveCar(Car car){
      return repository.save(car);
    }

    public List<Car> saveListOfCar(List<Car> cars){
        return repository.saveAll(cars);
    }

    public List<Car> getCar(){
        return repository.findAll();
    }

    public Car getCarById(int id ){
        return repository.findById(id).orElse(null);
    }

    public Car getCarByName(String name ){
        return repository.findByName(name);
    }

    public String deleteCar(int id){
        repository.deleteById(id);
        return "Car is deleted "+id;
    }

    public Car updateCar(Car car){
        Car existing = repository.findById(car.getId()).orElse(null);
        existing.setName(car.getName());
        return repository.save(existing);
    }

}
